" --------------------------------------------------------------------------- "
" Name: vimrc
" Author: em-winterschon
" Date: 2024-02-06
" Version: 1.8
" --------------------------------------------------------------------------- "

" Vim Plugin Handler
" --------------------------------------------------------------------------- "
" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
            \| PlugInstall --sync | source $MYVIMRC
            \| endif

call plug#begin('~/.vim/plugged')

" Colorscheme Themes
" --------------------------------------------------------------------------- "
Plug 'em-winterschon/modblush.vim'

" Everything else
" --------------------------------------------------------------------------- "
Plug 'maxbrunsfeld/vim-emacs-bindings'  " Keybindings for sanity
Plug 'ctrlpvim/ctrlp.vim'               " Fuzzy filename search
Plug 'DavidEGx/ctrlp-smarttabs'         " Add tabs to ctrl-p
Plug 'dense-analysis/ale'               " syntax checking and semantic errors
Plug 'scrooloose/nerdtree'              " file system explorer
Plug 'ryanoasis/vim-devicons'           " file icons in nerdtree
Plug 'tpope/vim-surround'               " surroundings (parenthesis, brackets, quotes, ...)
Plug 'nathanaelkane/vim-indent-guides'  " visual indent levels
Plug 'editorconfig/editorconfig-vim'    " .editorconfig support
Plug 'prabirshrestha/asyncomplete.vim'  " autocomplete
Plug 'itchyny/lightline.vim'            " status bar
Plug 'maximbaz/lightline-ale'           " status bar syntax checking
Plug 'nickspoons/vim-sharpenup'         " mappings, code-actions available flag and statusline integration
Plug 'tpope/vim-obsession'              " sessions
Plug 'romainl/vim-qf'                   " quickfix fixes
Plug 'vim-autoformat/vim-autoformat'    " code formatting

call plug#end()

" Mapped Calls
" --------------------------------------------------------------------------- "
":map <F4> :%s/\t//g<CR>
:map <F4> :%!expand -t 2<CR>
:map <F5> :Autoformat<CR>

" Functions
" --------------------------------------------------------------------------- "
function CheatsheetFilter(id, key)
    if a:key == "q"
        call popup_close(a:id)
        return v:true
    endif
    return v:false
endfunction

function Cheatsheet()
    call popup_create([
                \    " buffer:     ⇥ o: open         ·   ⇥ p: open tab     . ⇥ e: tree toggle  ·   ⇥ f: open tree    . ⇥ x: exit",
                \    "             ⇥ t: new tab      ·   ⇥ 1: left tab     . ⇥ 2: right tab    .   ⇥ s: save         . ⇥ w: close",
                \    "",
                \    " split:      ⇥ =: split horiz  · ⇥ |: split vertic",
                \    "             ⇥ ←: left split   . ⇥ →: right split    . ⇥ ↑: up split     .   ⇥ ↓: split down",
                \    "             ⇥ j: split size ← . ⇥ l: split size →   . ⇥ i: split size ↑ .   ⇥ m: split size ↓",
                \    "",
                \    " navigate:   C-u: page up      .   C-d: page down    ·   H: top          ·     M: middle       ·   L: bottom",
                \    "               B: full word ←  ·     W: full word →  ·   b: word ←       ·     w: word →",
                \    "               0: start        ·     ^: first        ·   $: end          ·    gg: fof          ·   G: eof",
                \    "              F8: quickfix     ·    F9: locations    ·   F10: buffers    ·   F12: clean search",
                \    "",
                \    " highlight:  ⇥ h: highl line   ·   ⇥ c: clear highl",
                \    "",
                \    " .net:        F2: rename       ·    F3: peek def     ·  F4: goto def     ·    F5: impl         ·  F6: usages",
                \    "              F7: code issues",
                \    "",
                \    " tmux:     ⌥ ⌘ ↑: up           · ⌥ ⌘ ↓: down         · ⌥ ⌘ ←: left       · ⌥ ⌘ →: right",
                \    "           S ⌘ ↑: size up      · S ⌘ ↓: size down    · S ⌘ ←: size left  · S ⌘ →: size right",
                \ ], #{
                \    title: ' Cheatsheet (q to close)',
                \    pos: 'center',
                \    padding: [],
                \    close: 'click',
                \    line: 1,
                \    border: [],
                \    zindex: 300,
                \    filter: 'CheatsheetFilter'
                \ })
endfunction

" General settings
" --------------------------------------------------------------------------- "
filetype indent plugin on
if !exists('g:syntax_on') | syntax enable | endif

scriptencoding utf-8
set backspace=indent,eol,start
set completeopt=menuone,noinsert,noselect,popuphidden
set completepopup=highlight:Pmenu,border:on
set encoding=utf-8
set expandtab
set hidden
set history=256
set hlsearch
set incsearch
set invlist
set laststatus=2
set linebreak
" set list
set listchars=tab:▷.,eol:¬,trail:·,nbsp:·,extends:>,precedes:<
set noerrorbells
set nofixendofline
set noshowmode
set nostartofline
set number
set paste
set ruler
set shiftround
set shiftwidth=2
set showmatch      " Show matching brackets/braces/parantheses
set showtabline=2  " Always show tab line
set signcolumn=yes
set smartcase
set smartindent
set softtabstop=2
set splitbelow
set splitright
set tabstop=2
set textwidth=80
set ttimeout
set ttimeoutlen=100
set timeoutlen=3000
set title
set updatetime=1000
set wildignore+=*/tmp/*,*/obj/*,*.so,*.swp,*.zip
set wildmode=longest,list,full " Let TAB completion behave like bash's
set wildoptions=pum
set wrap

" Use truecolor in the terminal, when it is supported
" if has('termguicolors') | set termguicolors | endif
set background=dark

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
" r: nearest path ancestor with .git/, a: directory of the current file, unless
" it is a subdirectory of the cwd
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_root_markers = ['.csproj']
let g:ctrlp_custom_ignore = {
            \ 'dir':  '\v[\/]\.(git|hg|svn|obj)$',
            \ 'file': '\v\.(exe|so|dll|swo|swp)$'
            \ }
" ignore files in .gitignore
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
let g:ctrlp_extensions = ['smarttabs']
let g:ctrlp_smarttabs_modify_tabline = 1
let g:ctrlp_smarttabs_exclude_quickfix = 1

let g:ale_sign_error = '•'
let g:ale_sign_warning = '•'
let g:ale_sign_info = '·'
let g:ale_sign_style_error = '·'
let g:ale_sign_style_warning = '·'

" let g:indent_guides_enable_on_vim_startup = 1
" let g:indent_guides_exclude_filetypes = ['nerdtree']

let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']

let g:syntastic_auto_loc_list = 1 " Auto-open error window if errors are detected
let g:syntastic_enable_signs = 1
let g:syntastic_c_check_header = 1
let g:syntastic_c_no_include_search = 1 " Disable the search of included header files after special libraries
let g:syntastic_c_auto_refresh_includes=1

let g:asyncomplete_auto_popup = 1
let g:asyncomplete_auto_completeopt = 0

let g:modblush_transp_bg = 0
let g:lightline = {'colorscheme': 'modblush.vim'}

" Use unicode chars for ale indicators in the statusline
let g:lightline#ale#indicator_checking = "\uf110 "
let g:lightline#ale#indicator_infos = "\uf129 "
let g:lightline#ale#indicator_warnings = "\uf071 "
let g:lightline#ale#indicator_errors = "\uf05e "
let g:lightline#ale#indicator_ok = "\uf00c "

" quickfix and location windows
let g:qf_auto_open_quickfix = 1
let g:qf_auto_open_loclist = 1
let g:qf_auto_resize = 1
let g:qf_auto_quit = 1
let g:qf_save_win_view = 1
let g:qf_shorten_path = 3

" Shortcuts
let mapleader = "/"

" "sudo" save:
cmap w!! w !sudo tee % >/dev/null

" Python3 location for vim-autoindent
let g:python3_host_prog="/usr/bin/python3"

" tab navigation
" --------------------------------------------------------------------------- "
nnoremap <silent> <leader>1 :tabprevious<CR>
noremap <silent> <leader>2 :tabnext<CR>
nnoremap <silent> <leader>t :tabnew<CR>
inoremap <silent> <leader>t <Esc>:tabnew<CR>
noremap <silent> <leader>t <Esc>:tabnew<CR>
nmap <silent> <leader>w :q<CR>
imap <silent> <leader>w <Esc>:q<CR>
map <silent> <leader>w :q<CR>
nmap <silent> <leader>s :w<CR>
imap <silent> <leader>s <Esc>:w<CR>
map <silent> <leader>s :w<CR>
nmap <silent> <leader>o :CtrlP<CR>
imap <silent> <leader>o <Esc>:CtrlP<CR>
map <silent> <leader>o :CtrlP<CR>
nmap <silent> <leader>p :CtrlPSmartTabs<CR>
imap <silent> <leader>p <Esc>:CtrlPSmartTabs<CR>
map <silent> <leader>p :CtrlPSmartTabs<CR>
noremap <silent> <C-o> :CtrlPSmartTabs<CR>
nmap <silent> <leader>x :qa<CR>
imap <silent> <leader>x <Esc>:qa<CR>
map <silent> <leader>x :qa<CR>

" Split navigation
" --------------------------------------------------------------------------- "
nnoremap <silent> <leader>= :split<CR>
nnoremap <silent> <leader>\ :vsplit<CR>
nnoremap <silent> <leader><Up> <C-W>k
nnoremap <silent> <leader><Down> <C-W>j
nnoremap <silent> <leader><Left> <C-W>h
nnoremap <silent> <leader><Right> <C-W>l
nnoremap <silent> <leader>j <C-W><
nnoremap <silent> <leader>l <C-W>>
nnoremap <silent> <leader>i <C-W>-
nnoremap <silent> <leader>m <C-W>+

" Line highlight
" --------------------------------------------------------------------------- "
nnoremap <silent> <Leader>h :call matchadd('LineHighlight', '\%'.line('.').'l')<CR>
nnoremap <silent> <Leader>c :call clearmatches()<CR>

" More navigation
" --------------------------------------------------------------------------- "
nnoremap <silent> <F1> :call Cheatsheet()<CR>
nnoremap <silent> <F8> :call qf#toggle#ToggleQfWindow(0)<CR>
nnoremap <silent> <F9> :call qf#toggle#ToggleLocWindow(0)<CR>
nnoremap <silent> <F10> :buffers<CR>:buffer<Space>
nnoremap <silent> <F12> :noh<cr>

" Nerdtree shortcuts
" --------------------------------------------------------------------------- "
nnoremap <silent> <leader>e :NERDTreeToggle<CR>
nnoremap <silent> <leader>f :NERDTreeFind<CR>

" navigation
" --------------------------------------------------------------------------- "
nmap <silent> <C-b> b
imap <silent> <C-b> <Esc>b
vmap <silent> <C-b> b
nmap <silent> <C-f> w
imap <silent> <C-f> <Esc>w
vmap <silent> <C-f> w

" shift+arrow selection
" --------------------------------------------------------------------------- "
nmap <silent> <S-Up> v<Up>
nmap <silent> <S-Down> v<Down>
nmap <silent> <S-Left> v<Left>
nmap <silent> <S-Right> v<Right>
vmap <silent> <S-Up> <Up>
vmap <silent> <S-Down> <Down>
vmap <silent> <S-Left> <Left>
vmap <silent> <S-Right> <Right>
imap <silent> <S-Up> <Esc>v<Up>
imap <silent> <S-Down> <Esc>v<Down>
imap <silent> <S-Left> <Esc>v<Left>
imap <silent> <S-Right> <Esc>v<Right>

" clipboard support
" --------------------------------------------------------------------------- "
vnoremap <silent> <C-c> :w !pbcopy<CR><CR>
noremap <silent> <C-v> :r !pbpaste<CR><CR>

" Remove trailing white spaces on save
" --------------------------------------------------------------------------- "
autocmd BufWritePre * :%s/\s\+$//e

" Mimic Emacs Line Editing in Insert Mode Only
" --------------------------------------------------------------------------- "
"inoremap <C-A> <Home>
"inoremap <C-B> <Left>
"inoremap <C-E> <End>
"inoremap <C-F> <Right>
"inoremap â <C-Left>
"inoremap æ <C-Right>
"inoremap <C-K> <Esc>lDa
"inoremap <C-U> <Esc>d0xi
"inoremap <C-Y> <Esc>Pa
"inoremap <C-X><C-S> <Esc>:w<CR>a

" --------------------------------------------------------------------------- "
" Options for 'autostart mode insert' and 'modify selmodify cmd'
"  set insertmode
"  set selectmode+=cmd
" --------------------------------------------------------------------------- "
" CTRL-X to cut
vnoremap <C-X> "+x
" CTRL-C to copy
vnoremap <C-C> "+y
" CTRL-v to paste
map <C-V> "+gP
cmap <C-V> <C-R>+
" remap default CTRL-V to CTRL-Q
noremap <C-Q> <C-V>

" Delete trailing spaces plugin mappings
" --------------------------------------------------------------------------- "
nnoremap <Leader>d$ :<C-u>%DeleteTrailingWhitespace<CR>
vnoremap <Leader>d$ :DeleteTrailingWhitespace<CR>
let g:DeleteTrailingWhitespace = 1
let g:DeleteTrailingWhitespace_Action = 'delete'
let g:DeleteTrailingWhitespace_ChoiceAffectsHighlighting = ['No', 'Never', 'Never ever', 'Never ever recalled', 'Never ever highlight', 'Nowhere']

" Enable auto-indent syntax for shell files
" --------------------------------------------------------------------------- "
" filetype indent on
" autocmd BufRead,BufWritePre *.sh normal gg=G
" au BufWrite * :Autoformat

" --------------------------------------------------------------------------- "
" END
" --------------------------------------------------------------------------- "

