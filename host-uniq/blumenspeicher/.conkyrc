-- Name     : .conkyrc
-- Purpose  : Config file for Conky app
-- Author   : EM Winterschon
-- Repo     : https://github.com/em-winterschon/dotfiles
-- Requires : Conky installed
-- Date     : 2021-06-19
-- Version  : 1.1.0_blumenspiecher-RHEL8
-- Compat   : Conky 1.12.2_pre compiled from source on Linux 4.18.0-305.3.1.el8_4.x86_64

conky.config = {
	use_spacer = 'left',
	pad_percents = 3,
	background = false,
	double_buffer = true,
	use_xft = true,
	font = 'monospace:size=9',

	--## [start] use the following for a triple monitor: 4K top-middle, 4K top-left, 5K bottom
	alignment = 'top_right',
	gap_x = 10,
	gap_y = 20,
	--## [end]

	--## [start] use the following for a quad monitor: 4K top-right, 2K top-left, 2K bottom left+right
	-- alignment = 'top_left',
	-- gap_x = -700,
	-- gap_y = 2080,
	--## [end]

	--## [start] use the following for a single monitor
	-- alignment = 'top_left',
	-- gap_x = 10,
	-- gap_y = 0,
	--## [end]

	own_window = true,
	--own_window_type = desktop,
	own_window_argb_visual = true,
	own_window_argb_value = 0,
	--own_window_hints = 'above,skip_taskbar,skip_pager,sticky',
	own_window_hints = 'above,undecorated,below,skip_taskbar,skip_pager,sticky',
	update_interval = 5.0,
	lua_load = '~/.ping.lua',
};

conky.text = [[

${color magenta}Hostname: ${color}${nodename}
${color magenta}NOW():    ${color}${time %A %F %T %z %Z}
${color magenta}Kernel:   ${color}${sysname} ${kernel}
${color magenta}Uptime:   ${color}${uptime}
${color magenta}Profile:  ${color}${texeci 300 tuned-adm profile_info | head -n 2 | tail -n 1}
${hr}
${color magenta}CPU:${color} ${freq_g} GHz | Dual ${texeci 300 lscpu | grep "Model name:" | awk '{print $6,$7,$8,$9}' | tr -d '()' | sed 's/R//g'}
#${color magenta}SUM${color}${cpu cpu0}%
#${cpubar cpu0 6}
#${color magenta}CPU Spec:${color} ${freq_g} GHz T-State | ${execi 300 lscpu | grep "Model name:" | awk '{print "Dual "$3,$4,$6$7,$9}' | tr -d '()' | sed 's/R//g'}
${color magenta}CPU Core Temps 0,1:${color} ${execi 60 sudo ipmitool sdr | egrep -i "CPU1 Temp" | awk '{print $4"C"}'}, ${execi 60 sudo ipmitool sdr | egrep -i "CPU2 Temp" | awk '{print $4"C"}'}
${color magenta}CPU Fan Speeds 0,1:${color} ${execi 60 sudo ipmitool sdr | egrep -i "FANA" | awk '{print $3,$4}'}, ${execi 60 sudo ipmitool sdr | egrep -i "FANA" | awk '{print $3,$4}'}
${color magenta}AVG Fan Speeds F,R:${color} ${execi 60 sudo ipmitool sdr | egrep -i "FAN3" | awk '{print $3,$4}'}, ${execi 60 sudo ipmitool sdr | egrep -i "FAN5" | awk '{print $3,$4}'}
${hr}
${color magenta}SUM${color}${cpu cpu0}% ${cpubar cpu0 6}
${color magenta}01:${color}${cpu cpu1}% ${cpubar cpu1 6,238} ${color magenta}09:${color}${cpu cpu9}% ${cpubar cpu9 6,238}
${color magenta}02:${color}${cpu cpu2}% ${cpubar cpu2 6,238} ${color magenta}10:${color}${cpu cpu10}% ${cpubar cpu10 6,238}
${color magenta}03:${color}${cpu cpu3}% ${cpubar cpu3 6,238} ${color magenta}11:${color}${cpu cpu11}% ${cpubar cpu11 6,238}
${color magenta}04:${color}${cpu cpu4}% ${cpubar cpu4 6,238} ${color magenta}12:${color}${cpu cpu12}% ${cpubar cpu12 6,238}
${color magenta}05:${color}${cpu cpu5}% ${cpubar cpu5 6,238} ${color magenta}13:${color}${cpu cpu13}% ${cpubar cpu13 6,238}
${color magenta}06:${color}${cpu cpu6}% ${cpubar cpu6 6,238} ${color magenta}14:${color}${cpu cpu14}% ${cpubar cpu14 6,238}
${color magenta}07:${color}${cpu cpu7}% ${cpubar cpu7 6,238} ${color magenta}15:${color}${cpu cpu15}% ${cpubar cpu15 6,238}
${color magenta}08:${color}${cpu cpu8}% ${cpubar cpu8 6,238} ${color magenta}16:${color}${cpu cpu16}% ${cpubar cpu16 6,238}
${cpugraph}
${color magenta}Memory:         Used/Total          Free          Swap Used/Total
${color}         ${mem}/${memmax}      ${memfree}        ${swap}/${swapmax}
${memperc}% ${membar 4}
${memgraph}
${color magenta}Load average: ${color}${loadavg}     ${color magenta}Processes: ${color}${processes}   ${color magenta}Running:${color} ${running_processes}
${color magenta}Name              PID     CPU%   MEM%
${color lightgrey} ${top name 1} ${top pid 1} ${top cpu 1} ${top mem 1}
${color lightgrey} ${top name 2} ${top pid 2} ${top cpu 2} ${top mem 2}
${color lightgrey} ${top name 3} ${top pid 3} ${top cpu 3} ${top mem 3}
${color lightgrey} ${top name 4} ${top pid 4} ${top cpu 4} ${top mem 4}
${color lightgrey} ${top name 5} ${top pid 5} ${top cpu 5} ${top mem 5}
${hr}
${color magenta}IPaddr [wired:ens3]:   ${color lightgrey}${addr ens3}
${color magenta}Up: ${color lightgrey}${upspeed ens3}/s
${upspeedgraph ens3}
${color magenta}Down: ${color lightgrey}${downspeed ens3}/s
${downspeedgraph ens3}
${hr}
${color magenta}IPaddr [vpn:wgpia0]:   ${color lightgrey}${addr wgpia0}
${color magenta}Up: ${color lightgrey}${upspeed wgpia0}/s
${upspeedgraph wgpia0}
${color magenta}Down: ${color lightgrey}${downspeed wgpia0}/s
${downspeedgraph wgpia0}
${hr}
${color magenta}IPaddr [vpn:nebula1]:   ${color lightgrey}${addr nebula1}
${color magenta}Up: ${color lightgrey}${upspeed nebula1}/s
${upspeedgraph nebula1}
${color magenta}Down: ${color lightgrey}${downspeed nebula1}/s
${downspeedgraph nebula1}
${hr}
${color magenta}IPaddr    Public:    ${color lightgrey}${texeci 60 /usr/bin/curl --silent --fail https://ipecho.net/plain}, ${tcp_ping ipecho.net 443} ms
${color magenta}LAN  SFO-000-EIN:    ${color lightgrey}${texeci 10 ~/.pingtest.sh 10.99.99.1}, ${tcp_ping 10.99.99.1 443} ms
${color magenta}SSH  SFO-200-PRI:    ${color lightgrey}${texeci 60 ~/.pingtest.sh 10.200.99.2}, ${tcp_ping 10.200.99.2 6432} ms
${color magenta}SSH   SQL133-OOB:    ${color lightgrey}${texeci 60 ~/.pingtest.sh oob-sql13307.hosts.secretcdn.net}, ${tcp_ping oob-sql13307.hosts.secretcdn.net 22} ms
${color magenta}DNS      1.1.1.1:    ${color lightgrey}${texeci 30 ~/.pingtest.sh 1.1.1.1}, ${tcp_ping 1.1.1.1 53} ms
${color magenta}DNS      8.8.8.8:    ${color lightgrey}${texeci 30 ~/.pingtest.sh 8.8.8.8}, ${tcp_ping 8.8.8.8 53} ms
${color magenta}DNS      9.9.9.9:    ${color lightgrey}${texeci 30 ~/.pingtest.sh 9.9.9.9}, ${tcp_ping 9.9.9.9 53} ms
${color magenta}HTTPS     Google:    ${color lightgrey}${texeci 30 ~/.pingtest.sh google.com}, ${tcp_ping google.com 443} ms
${hr}
${color magenta}Mount        Used / Total
${color magenta}root-fs:     ${color}${fs_used /}/${fs_size /} ${fs_bar 6 /}
${color magenta}sys-tmp-fs:  ${color}${fs_used /tmp}/${fs_size /tmp} ${fs_bar 6 /tmp}
${color magenta}user-tmp-fs: ${color}${fs_used /home/mwinterschon/tmp}/${fs_size /home/mwinterschon/tmp} ${fs_bar 6 /home/mwinterschon/tmp}
${hr}
${color magenta}OS-RAID
${color magenta}Device      Read IOPs        Write IOPs         ${color magenta}Temp/(Dev)
${color magenta}NVMe2n1    ${color}${diskio_read nvme2n1p1}/s        ${diskio_write nvme2n1p1}/s     ${texeci 10 ~/bin/bash/smartctl.temps.sh nvme2n1}
${color magenta}NVMe3p1    ${color}${diskio_read nvme3n1p1}/s        ${diskio_write nvme3n1p1}/s     ${texeci 10 ~/bin/bash/smartctl.temps.sh nvme3n1}

${color magenta}OS-SWAP
${color magenta}Device      Read IOPs        Write IOPs
${color magenta}md126      ${color}${diskio_read md126}/s            ${diskio_write md126}/s
#${hr}
#${color magenta}ZFS Pools
#${color}${texeci 10 /sbin/zpool list | head -n 1 | awk '{print $1"\t\t"$2"\t"$3"\t"$4"\t"$7"\t"$9}'}
#${color}${texeci 10 /sbin/zpool list | tail -n 1 | awk '{print $1"\t\t"$2"\t"$3"\t"$4"\t"$7"\t"$9}'}
${hr}
${color magenta}CPU Temp   LGA-0 ${color lightgrey} ${color magenta}  LGA-1                LGA-0 ${color lightgrey} ${color magenta}  LGA-1
${color magenta}Core 01:   ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 0:' | cut -c17-23 | sed '/^$/d' | head -n 1}| ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 0:' | cut -c17-23 | sed '/^$/d' | tail -n 1} | ${color magenta}Core 05:   ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 4:' | cut -c17-23 | sed '/^$/d' | head -n 1}| ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 4:' | cut -c17-23 | sed '/^$/d' | tail -n 1}
${color magenta}Core 02:   ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 1:' | cut -c17-23 | sed '/^$/d' | head -n 1}| ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 1:' | cut -c17-23 | sed '/^$/d' | tail -n 1} | ${color magenta}Core 06:   ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 5:' | cut -c17-23 | sed '/^$/d' | head -n 1}| ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 5:' | cut -c17-23 | sed '/^$/d' | tail -n 1}
${color magenta}Core 03:   ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 2:' | cut -c17-23 | sed '/^$/d' | head -n 1}| ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 2:' | cut -c17-23 | sed '/^$/d' | tail -n 1} | ${color magenta}Core 07:   ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 6:' | cut -c17-23 | sed '/^$/d' | head -n 1}| ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 6:' | cut -c17-23 | sed '/^$/d' | tail -n 1}
${color magenta}Core 04:   ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 3:' | cut -c17-23 | sed '/^$/d' | head -n 1}| ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 3:' | cut -c17-23 | sed '/^$/d' | tail -n 1} | ${color magenta}Core 08:   ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 7:' | cut -c17-23 | sed '/^$/d' | head -n 1}| ${color lightgrey}${execi 10 /bin/sensors | grep 'Core 7:' | cut -c17-23 | sed '/^$/d' | tail -n 1}
${hr}
${color magenta}HDMI Capture Card - Core Temp: ${color}${texeci 300 mwcap-info -i /dev/video0 | grep "Chipset temperature" | awk '{print $4}'}
${hr}
${color magenta}EST:    ${color lightgrey}${tztime America/New_York} | ${color magenta}Paris: ${color lightgrey}${tztime Europe/Paris}
${color magenta}Sydney: ${color lightgrey}${tztime Australia/Sydney} | ${color magenta}UTC+0: ${color lightgrey}${tztime UTC}
${color magenta}PST-7:  ${color lightgrey}${tztime US/Pacific} | ${color magenta}UTC-1: ${color lightgrey}${tztime UTC-1}
${hr}
${color magenta}FSLY:   ${color lightgrey}${execi 300 echo -n "$" && curl --silent --fail -X GET https://query1.finance.yahoo.com/v10/finance/quoteSummary/FSLY?modules=price | jq '.quoteSummary .result[0] .price.regularMarketPrice.fmt' | sed -e 's/"//g'} | Change: ${execi 300 curl --silent --fail -X GET https://query1.finance.yahoo.com/v10/finance/quoteSummary/FSLY?modules=price | jq '.quoteSummary .result[0] .price.regularMarketChangePercent.fmt' | sed -e 's/"//g'}
${hr}
]];
